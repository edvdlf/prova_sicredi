package br.edl.sistemas.desafioautomacao.core;


import static br.edl.sistemas.desafioautomacao.core.WebDriverFactory.getDriver;
import static br.edl.sistemas.desafioautomacao.core.WebDriverFactory.killDriver;


import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;




public class SeleniumHelper {
	
	
	public static final int DEFAULT_DELAY = 1000;
	
	
	public void IrParaHome() {
		
		getDriver().get(Property.SITE_ADDRESS);

	}

	public String obterTituloPagina() {
		return getDriver().getTitle().toString();
	}

	/********* Text ************/

	public void preencherTextBoxPorId(String idCampo, String valorCampo) {
		escrever(By.id(idCampo), valorCampo);
	}
	
	public void preencherTextBoxPorName(String nameCampo, String valorCampo) {
		getDriver().findElement(By.name(nameCampo));
	}

	private void escrever(By by, String texto) {

		getDriver().findElement(by).clear();
		getDriver().findElement(by).sendKeys(texto);
	}

	/********* Combo ************/

	public void selecionarCombo(String id, String valor) {
		WebElement element = getDriver().findElement(By.id(id));
		Select combo = new Select(element);
		combo.selectByVisibleText(valor);
	}

	/********* xPath Css ************/

	public WebElement obterElementoPorXPath(String xPath) {
		WebElement elemento = getDriver().findElement(By.xpath(xPath));
		return elemento;
	}
	public WebElement obterElementoPorXPath(String xPath, int segundos) {
		WebElement elemento = getDriver().findElement(By.xpath(xPath));
		WebDriverWait wait = new WebDriverWait(getDriver(), segundos, DEFAULT_DELAY);
		return wait.until(ExpectedConditions.elementToBeClickable(elemento));
	}
	
	public WebElement obterElementoPorName(String name, int segundos) {
		WebElement elemento = getDriver().findElement(By.name(name));
		WebDriverWait wait = new WebDriverWait(getDriver(), segundos, DEFAULT_DELAY);
		return wait.until(ExpectedConditions.elementToBeClickable(elemento));
	}


	public WebElement obterElementoPorCssSelector(String cssSelector) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10, DEFAULT_DELAY);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssSelector)));
		WebElement elemento = getDriver().findElement(By.cssSelector(cssSelector));
		return elemento;
	}
	
	public static WebElement waitForElementToBeClickable(WebElement webElement, int seconds) {
		WebDriverWait wait = new WebDriverWait(getDriver(), seconds, DEFAULT_DELAY);
		return wait.until(ExpectedConditions.elementToBeClickable(webElement));
	}
	
	

	/********* Botao ************/

	public void clicarBotaoPorId(String id) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
		clicarBotao(By.id(id));
	}

	private void clicarBotao(By by) {
		getDriver().findElement(by).click();
	}

	public void clicarBotaoPorCssSelector(String cssSelector) {

		getDriver().findElement(By.cssSelector(cssSelector)).click();
	}

	public void clicarBotaoPorTexto(String texto) {
		clicarBotao(By.xpath("//button[.='" + texto + "']"));
	}
	
/********* Link ************/
	
	public void clicarLink(String link) {
		getDriver().findElement(By.linkText(link)).click();
	}
	public void duploCliqueLink(String link) {
		getDriver().findElement(By.linkText(link)).click();
		getDriver().findElement(By.linkText(link)).click();
	}
	
	public void clicarLink(String link, int tempo) {
		WebDriverWait wait = new WebDriverWait(getDriver(),tempo);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(link)));
		clicarLink(link);
	}

	/********* Modal ************/

	public String obterTextoModalPorCssSelector(String cssSelector) {
		String men = "";
		
		try {
			men = getDriver().findElement(By.cssSelector(cssSelector)).getText();
			System.out.println(men);

			
		} catch (Exception ex) {
			ex.getMessage();
		}
		return men;
	}
	
	/********* Texto tela ************/
	
	public String verificarTextoPorTagName(String tagName, String texto) {
		String men = "";
		boolean	menExiste=false;
		try {
		menExiste = getDriver().findElement(By.tagName(tagName)).getText().contains(texto);
		} catch (Exception ex) {
			ex.getMessage();
		}
		if(menExiste==true) {
			men=texto;
		}
		return men;
	}
	

	/********* Alerts ************/

	public String alertaObterTexto() {
		Alert alert = getDriver().switchTo().alert();
		return alert.getText();
	}

	public String alertaObterTextoEAceita() {
		Alert alert = getDriver().switchTo().alert();
		String valor = alert.getText();
		alert.accept();
		return valor;

	}

	public String alertaObterTextoENega() {
		Alert alert = getDriver().switchTo().alert();
		String valor = alert.getText();
		alert.dismiss();
		return valor;

	}

	public void fecharNavegador() {
		killDriver();
	}

	
	
	

}
