package br.edl.sistemas.desafioautomacao.pages;

import br.edl.sistemas.desafioautomacao.core.SeleniumHelper;

public class AddCustomerPage extends SeleniumHelper {
	
	public AddCustomerPage() {
		
	}
	
	public void AcessarHomePage() {
		IrParaHome();
	}

	public void SelecionarVersaoBoostrap(String versao) {
		selecionarCombo("switch-version-select", versao);
	}

	public void ClicarNoBotaoAddRecord(){
		obterElementoPorXPath("//a[contains(@href, '/v1.x/demo/my_boss_is_in_a_hurry/bootstrap-v4/add')]", 10).click();
	}
	
	public void PreencherFormularioAddRecord() {
		preencherTextBoxPorId("field-customerName", "Teste Sicredi" );
		preencherTextBoxPorId("field-contactLastName","Teste" );
		preencherTextBoxPorId("field-contactFirstName", "Edvaldo Alves" );
		preencherTextBoxPorId("field-phone", "51 9999-9999" );
		preencherTextBoxPorId("field-addressLine1", "Av Assis Brasil, 3970");
		preencherTextBoxPorId("field-addressLine2","Torre D" );
		preencherTextBoxPorId("field-city","Porto Alegre" );
		preencherTextBoxPorId("field-state","RS" );
		preencherTextBoxPorId("field-postalCode", "91000-000");
		preencherTextBoxPorId("field-country","Brasil" );
		preencherTextBoxPorId("field-salesRepEmployeeNumber","Fixter" );
		preencherTextBoxPorId("field-creditLimit", "200");
		
	    

	}
	
	public void ClicarBotaoSave() {
		clicarBotaoPorId("form-button-save");
		
	}
	
	public String ObterMensagemSucesso(String mensagem) {
		
		String men = verificarTextoPorTagName("p", mensagem);
		
		return men;
	}
	
	public void ClicarlinkGoBackToList(String link) {
		clicarLink(link);
	}
	
	public void PreencherCamposParaExclusao() throws InterruptedException {
		obterElementoPorName("customerName", 10).sendKeys("Teste Sicredi");
		Thread.sleep(1000);
		obterElementoPorCssSelector("input.select-all-none").click();
		Thread.sleep(500);
		obterElementoPorCssSelector("input.select-all-none").click();
		Thread.sleep(500);
		obterElementoPorCssSelector("input.select-row").click();
		Thread.sleep(500);
		obterElementoPorCssSelector("span.text-danger").click();
		Thread.sleep(1000);

		Thread.sleep(1000);
		
		
	}
	
	public String ObterMensagemExclusao() {
		String men = "";
		
		try {
			men = obterTextoModalPorCssSelector("p.alert-delete-multiple");
			
		}catch(Exception ex){
			System.out.println(ex);
		}
		
		return men;
	}
	
	public void ClicarBotaoDelete() {
		clicarBotaoPorCssSelector("button.btn.btn-danger.delete-multiple-confirmation-button");
		
	}
	
	
	public Boolean AddRecord_ValidarTitulo(String tituloPagina) {
		Boolean tituloValido = false;
		String resultado = obterTituloPagina();
		if (resultado == tituloPagina) {
			tituloValido = true;
		}
		return tituloValido;
	}

}
