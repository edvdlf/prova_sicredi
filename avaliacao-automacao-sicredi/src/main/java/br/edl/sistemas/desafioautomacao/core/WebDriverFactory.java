package br.edl.sistemas.desafioautomacao.core;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;



public class WebDriverFactory {
	
	
	private static WebDriver driver;
	private static final WebDriverWait wait = null;
	
	
	private WebDriverFactory(){
		
	}
	
	public static WebDriver getDriver() {
		
		String browser = Property.BROWSER_NAME;
		String s = Property.CHROME_DRIVE_PATH;
		
		if (driver == null) {
			if (Browser.CHROME.equals(browser)) {
				System.setProperty("webdriver.chrome.driver", Property.CHROME_DRIVE_PATH);
				driver = new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
			
			}else  if (Browser.FIREFOX.equals(browser)){
				System.setProperty("webdriver.chrome.driver", Property.FIREFOX_DRIVE_PATH);
				driver = new FirefoxDriver();
			}
		}
		return driver;
	}
	
	public static void killDriver() {
		if(driver !=null) {
			driver.quit();
			driver=null;
		}
	}

}
