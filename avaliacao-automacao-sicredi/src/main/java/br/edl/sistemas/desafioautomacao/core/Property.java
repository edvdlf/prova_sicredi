package br.edl.sistemas.desafioautomacao.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public abstract class Property {

	public static String CHROME_DRIVE_PATH;
	public static String FIREFOX_DRIVE_PATH;

	public static String BROWSER_NAME;
	public static String SITE_ADDRESS;

	private static final String PROPERTIES_FILE = "src\\main\\resources\\config\\config.properties";

	static Properties prop = getProp();

	static {
		CHROME_DRIVE_PATH = new File("").getAbsolutePath() + "\\src\\main\\resources\\webdrivers\\chromedriver.exe";
		FIREFOX_DRIVE_PATH = new File("").getAbsolutePath() + "\\src\\main\\resources\\webdrivers\\geckodriver.exe";

		BROWSER_NAME = prop.getProperty("browser.name");
		SITE_ADDRESS = prop.getProperty("site.address");
	}

	public static Properties getProp() {
		Properties props = new Properties();
		try {

			FileInputStream file = new FileInputStream(PROPERTIES_FILE);
			props.load(file);
			return props;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return props;
	}
}
