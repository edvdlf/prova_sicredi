package br.edl.sistemas.desafioauromacao.steps;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Assert;

import br.edl.sistemas.desafioautomacao.pages.AddCustomerPage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class AddCustomerStep {

	AddCustomerPage addCustomerPage;
	String mensagemSucessoEsperada = "Your data has been successfully stored into the database.";
	String mensagemSucessoObtida = "";

	String mensagemDeleteObtida = "";

	public AddCustomerStep() {
		addCustomerPage = new AddCustomerPage();
	}

	@Dado("que acessei o site")
	public void que_acessei_o_site() {
		addCustomerPage.AcessarHomePage();
	}

	@Quando("eu selecionar a opcao {string}")
	public void eu_selecionar_a_opcao(String versao) {
		addCustomerPage.SelecionarVersaoBoostrap(versao);
	}

	@Quando("clicar no botao Add Record")
	public void clicar_no_botao_Add_Record() throws InterruptedException {
		addCustomerPage.ClicarNoBotaoAddRecord();
	}

	@Quando("preencher os campos do formulario Add Record")
	public void preencher_os_campos_do_formulario_Add_Record() {
		addCustomerPage.PreencherFormularioAddRecord();
	}

	@Quando("clicar no botao Salvar")
	public void clicar_no_botao_Salvar() {
		addCustomerPage.ClicarBotaoSave();
	}

	@Entao("devo ver a mensagem {string}")
	public void devo_ver_a_mensagem(String mensagem) {
		mensagemSucessoObtida = addCustomerPage.ObterMensagemSucesso(mensagem);

		Assert.assertEquals(mensagemSucessoEsperada, mensagemSucessoObtida);

	}

	@Entao("quando eu clicar no link {string}")
	public void quando_eu_clicar_no_link(String link) {

		addCustomerPage.ClicarlinkGoBackToList(link);
	}

	@Entao("preencher os campos para exclusao")
	public void preencher_os_campos_para_exclusao() throws InterruptedException {
		addCustomerPage.PreencherCamposParaExclusao();
	}

	@Entao("eu devo ver a mensagem {string}")
	public void eu_devo_ver_a_mensagem(String mensagemDeleteEsperada) {

		mensagemDeleteObtida = addCustomerPage.ObterMensagemExclusao();

		Assert.assertEquals(mensagemDeleteEsperada, mensagemDeleteObtida);
	}

	@Entao("quando eu clicar  no botao Delete")
	public void quando_eu_clicar_no_botao_Delete() {

		addCustomerPage.ClicarBotaoDelete();
	}

	@Entao("eu devo ver a a seguinte mensagem {string}")
	public void eu_devo_ver_a_a_seguinte_mensagem(String mensagemEsperada) {
		
		
		
	}

	@Before
	public void inicio() {
		System.out.println("Iniciando testes feature: Add Customer!");
	}

	@After
	public void fecharBrowser() {
		addCustomerPage.fecharNavegador();
		System.out.println("Finalizado testes feature: Add Customer!");
	}

}
