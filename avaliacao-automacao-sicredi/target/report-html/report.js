$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/AddCustomer.feature");
formatter.feature({
  "name": "Adicionar Customer",
  "description": "\tComo um candidato a vaga de automatizador de testes\n\tQuero acessar o site, e adicionar um novo Customer \n\tPara realizar os passos descritos no desafio.",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "name": "@funcionais"
    }
  ]
});
formatter.scenario({
  "name": "Adicionar Novo Customer com Sucesso",
  "description": "",
  "keyword": "Cenario",
  "tags": [
    {
      "name": "@funcionais"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "que acessei o site",
  "keyword": "Dado "
});
formatter.match({
  "location": "AddCustomerStep.que_acessei_o_site()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "eu selecionar a opcao \"Bootstrap V4 Theme\"",
  "keyword": "Quando "
});
formatter.match({
  "location": "AddCustomerStep.eu_selecionar_a_opcao(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicar no botao Add Record",
  "keyword": "E "
});
formatter.match({
  "location": "AddCustomerStep.clicar_no_botao_Add_Record()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "preencher os campos do formulario Add Record",
  "keyword": "E "
});
formatter.match({
  "location": "AddCustomerStep.preencher_os_campos_do_formulario_Add_Record()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicar no botao Salvar",
  "keyword": "E "
});
formatter.match({
  "location": "AddCustomerStep.clicar_no_botao_Salvar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "devo ver a mensagem \"Your data has been successfully stored into the database.\"",
  "keyword": "Entao "
});
formatter.match({
  "location": "AddCustomerStep.devo_ver_a_mensagem(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "quando eu clicar no link \"Go back to list\"",
  "keyword": "E "
});
formatter.match({
  "location": "AddCustomerStep.quando_eu_clicar_no_link(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "preencher os campos para exclusao",
  "keyword": "E "
});
formatter.match({
  "location": "AddCustomerStep.preencher_os_campos_para_exclusao()"
});
formatter.result({
  "error_message": "org.openqa.selenium.StaleElementReferenceException: stale element reference: element is not attached to the page document\n  (Session info: chrome\u003d95.0.4638.54)\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/stale_element_reference.html\nBuild info: version: \u00273.11.0\u0027, revision: \u0027e59cfb3\u0027, time: \u00272018-03-11T20:26:55.152Z\u0027\nSystem info: host: \u0027LAPTOP-Q3PRALRQ\u0027, ip: \u0027172.25.160.1\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u002716.0.2\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 95.0.4638.54, chrome: {chromedriverVersion: 94.0.4606.61 (418b78f5838ed..., userDataDir: C:\\Users\\edvdl\\AppData\\Loca...}, goog:chromeOptions: {debuggerAddress: localhost:58630}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:extension:credBlob: true, webauthn:extension:largeBlob: true, webauthn:virtualAuthenticators: true}\nSession ID: 3b2b20f59a711f2c0d53d06bf5f40a8a\r\n\tat java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:78)\r\n\tat java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.base/java.lang.reflect.Constructor.newInstanceWithCaller(Constructor.java:499)\r\n\tat java.base/java.lang.reflect.Constructor.newInstance(Constructor.java:480)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:545)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:279)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.click(RemoteWebElement.java:83)\r\n\tat br.edl.sistemas.desafioautomacao.pages.AddCustomerPage.PreencherCamposParaExclusao(AddCustomerPage.java:64)\r\n\tat br.edl.sistemas.desafioauromacao.steps.AddCustomerStep.preencher_os_campos_para_exclusao(AddCustomerStep.java:68)\r\n\tat ✽.preencher os campos para exclusao(file:src/test/resources/features/AddCustomer.feature:16)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "eu devo ver a mensagem \"Are you sure that you want to delete this 1 item?\"",
  "keyword": "Entao "
});
formatter.match({
  "location": "AddCustomerStep.eu_devo_ver_a_mensagem(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "quando eu clicar  no botao Delete",
  "keyword": "E "
});
formatter.match({
  "location": "AddCustomerStep.quando_eu_clicar_no_botao_Delete()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "eu devo ver a a seguinte mensagem \"Your data has been successfully deleted from the database.\"",
  "keyword": "Entao "
});
formatter.match({
  "location": "AddCustomerStep.eu_devo_ver_a_a_seguinte_mensagem(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
});