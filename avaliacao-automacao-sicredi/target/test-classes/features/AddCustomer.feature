# language: pt
@funcionais
Funcionalidade: Adicionar Customer
	Como um candidato a vaga de automatizador de testes
	Quero acessar o site, e adicionar um novo Customer 
	Para realizar os passos descritos no desafio.
	
Cenario: Adicionar Novo Customer com Sucesso
Dado que acessei o site 
Quando eu selecionar a opcao "Bootstrap V4 Theme"
E clicar no botao Add Record
E preencher os campos do formulario Add Record
E clicar no botao Salvar
Entao devo ver a mensagem "Your data has been successfully stored into the database."
E quando eu clicar no link "Go back to list"
E preencher os campos para exclusao
Entao eu devo ver a mensagem "Are you sure that you want to delete this 1 item?"
E quando eu clicar  no botao Delete
Entao eu devo ver a a seguinte mensagem "Your data has been successfully deleted from the database."







	
